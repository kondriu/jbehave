package radio;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class Radio {
    private String stationName;
    private boolean turnedOn;
    private String currentStation;
    private String currentFrequency;
    private List<RadioStation> preset = new ArrayList<>();

    public void swithOnOff(){
        turnedOn = !turnedOn;
    }

    public boolean isTurnedOn(){
        return turnedOn;
    }

    public void selectStation(String stationName){
        currentStation = stationName;
    }

    public String getDisplay(){
        return getShortName(currentStation);
    }

    private String getShortName(String currentStation){
        if (currentStation==null){
            return "";
            }
        if (currentStation.length()>=10){
            return currentStation.substring(0, 7)+ "...";
        }
        return currentStation;
    }

    public void tuneTo (double frequency){
        currentFrequency = new DecimalFormat("#.00").format(frequency)+"PM";
    }

    public String getCurrentFrequency(){
        return currentFrequency;
    }

    public void addPreset(RadioStation station){
        preset.add(station);
    }
}
