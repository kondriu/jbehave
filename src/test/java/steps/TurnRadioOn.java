package steps;

import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;
import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.configuration.MostUsefulConfiguration;
import org.jbehave.core.io.LoadFromClasspath;
import org.jbehave.core.io.StoryLoader;
import org.jbehave.core.junit.JUnitStory;
import org.jbehave.core.reporters.Format;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

@RunWith(JUnitReportingRunner.class)
public class TurnRadioOn extends JUnitStory {


    @Override
    public Configuration configuration(){
        return new MostUsefulConfiguration()
                //where find a stories
                .useStoryLoader(new LoadFromClasspath(this.getClass()))
                .useStoryReporterBuilder(
                        //HTML, console and Text reporting
                        new StoryReporterBuilder().withDefaultFormats().withFormats(Format.CONSOLE, Format.TXT, Format.XML)
                                //stacktrace
                        .withFailureTrace(true).withFailureTraceCompression(true)
                );

    }

    //here we specify the steps classes
    @Override
    public InjectableStepsFactory stepsFactory(){
        //varargs, can have more than one steps class
        return new InstanceStepsFactory(configuration(), new RadioSteps());
    }
}
