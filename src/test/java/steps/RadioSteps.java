package steps;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import radio.Radio;
import radio.RadioStation;

import java.util.ArrayList;

import static org.junit.Assert.assertTrue;

public class RadioSteps {

    private Radio radio;
    private ArrayList<RadioStation> stations;

    @Given ("a digital radio")
    public void aDigitalRadio(){
        radio = new Radio();
    }

    @When("i press the on/off switch")
    public void iTurnedRadio(){
        radio.swithOnOff();
    }

    @Then("the radio should be turned on")
    public void theRadioShouldBeTurnedOn(){
        assertTrue(radio.isTurnedOn());
    }

}